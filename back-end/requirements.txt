django
djangorestframework
drf-yasg
django-cors-headers
psycopg2-binary

# dev
black
flake8